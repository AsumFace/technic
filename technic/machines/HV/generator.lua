minetest.register_alias("hv_generator", "technic:hv_generator")

minetest.register_craft({
	output = 'technic:hv_generator',
	recipe = {
		{'technic:carbon_plate',          'technic:lv_generator',   'technic:composite_plate'},
		{'default:diamondblock',              'technic:hv_transformer', 'default:diamondblock'},
		{'pipeworks:tube_1', 'technic:hv_cable',       'pipeworks:tube_1'},
	}
})

technic.register_generator({tier="HV", tube=1, supply=1200})

